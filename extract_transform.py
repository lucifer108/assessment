import csv
import json
from urllib import request

#Read CSV File
def readFile(file, jsonFile):
    with open(file, 'r') as csvfile:
        reader = csv.DictReader(csvfile)
        title = reader.fieldnames
        j = 0
        for row in reader:
            csvRows = [{title[i]:row[title[i]] for i in range(len(title))}]
            writeJSON(csvRows, jsonFile)

#Convert csv data into json, write to file, and send data to api
def writeJSON(data, jsonFile):
    with open(jsonFile, "a+") as f:
            f.write(json.dumps(data))
            sendData(data)

#Send data to api
def sendData(data) :
    headers = {"Content-Type": "application/json", "Authorization": "xxxxxx"}
    req = request.Request("https://api.capture.ie", data, headers)
    respone = request.urlopen(req)


readFile("input_data.csv", "outputFile.json")