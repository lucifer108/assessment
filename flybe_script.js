sendRequest({
    priceItinerary : getPriceDetails(),
    flightItinerary : getFlightLegDetails(),    
    passengerItinerary : getPassengerDetails(),
    serviceItinerary : getServiceDetails(),
    carItinerary : getCarDetails()
});

/*
 * Send flight data to requsted API as http POST request
*/
function sendRequest(flightData) {
    var request = new XMLHttpRequest();
    var url = "https://api.capturedata.ie";

    request.open('POST', url, true);
    request.setRequestHeader('Content-Type', 'application/json; chaset=UTF-8');

    request.onreadystatechange = function () {
        if(request.status >= 200 && request.status < 400) {
        } else {
            console.log("Unable to post data to API : " + request.status);
        }        
    }
    request.send(JSON.stringify(flightDatadata));
    //console.log(JSON.stringify(flightData));
}

/*
 * @return innerText from class as string
 */
function getClassText(element, className, alternativeClass = null) {
    return element.getElementsByClassName(className).length > 0 ? 
        element.getElementsByClassName(className)[0].innerText.replace(/\n/g, " ").trim() : element.getElementsByClassName(alternativeClass).length > 0 ? 
        element.getElementsByClassName(alternativeClass)[0].innerText.replace(/\n/g, " ").trim() : null
};

/*
 *Get price field as format {currency : "XXX", value : "XXX"}
 */
function getPriceWithCurrency(element, currencyClass, valueClass) {
    return {
        currency : element.getElementsByClassName(currencyClass).length > 0 ? element.getElementsByClassName(currencyClass)[0].title : null,
        value : element.getElementsByClassName(valueClass).length > 0 ? element.getElementsByClassName(valueClass)[0].innerText : null
    };
};

/*
 * Gets price details of flight if data exists
 * @return priceIntinerary object as JSON
 */
function getPriceDetails() {

    //Get tax subitem and assosicated details
    function getPriceBreakdown(priceDetails){    
        var taxDetailsArray = [];
        var taxBreakdown = priceDetails.getElementsByClassName("price-taxes-details-table")[1].getElementsByClassName("price-taxes-details-row clearfix");
        var taxHeader = priceDetails.getElementsByClassName("price-taxes-details-row price-taxes-total-traveller clearfix");
    
        for (j = 0; j < taxBreakdown.length; j++) {
            var taxDesc = getClassText(taxBreakdown[j], prTaxText);
            var price = getPriceWithCurrency(taxBreakdown[j], prTaxBreakdownPriceCur, prTaxBreakdownPriceVal);
            var taxInstance = {
                type : taxDesc,
                taxPrice : price
            }
            taxDetailsArray.push(taxInstance);
        }
    
        var taxBreakdownNode = {
            taxDescription : getClassText(taxHeader[0], prTaxText),
            taxSubPrice :  getPriceWithCurrency(taxHeader[0], prTaxTotalSubPriceCur, prTaxTotalSubPriceVal),
            taxBreakdown : taxDetailsArray
        };
        return taxBreakdownNode;
    };
    
    var prTotalPrice = "price-value price-total-amount-value";
    var prTotalDCCPrice = "price-value price-total-dcc-amount-value";
    var prTotalTaxCur = "price-currency taxesbreakdown-total-currency";
    var prTotalTaxVal = "price-value taxesbreakdown-total-value";
    var prTaxText = "price-taxes-details-text";
    var prTaxBreakdownPriceCur = "price-currency price-taxes-details-amount-currency";
    var prTaxBreakdownPriceVal = "price-value price-taxes-details-amount-value";
    var prTaxTotalSubPriceCur = "price-currency price-taxes-total-traveller-currency";
    var prTaxTotalSubPriceVal = "price-value price-taxes-total-traveller-value";
	
    if (document.querySelector("#fare-review-price") !== null) {
        var priceDetailsArray = [];
        var priceSummary = document.getElementById("fare-review-price").getElementsByClassName("price-taxes-details col-xs-24");
        var currencySelector = document.getElementById("tpl4_widget-input-purchaseForm-mcpForm-currencyOfferId");
        
        for (i = 0; i < priceSummary.length; i++) {
            var priceBreakdown = getPriceBreakdown(priceSummary[i]);
            priceDetailsArray.push(priceBreakdown);
        };
    
        return {
            totalPrice : getClassText(document, prTotalPrice, prTotalDCCPrice),
            totalPriceCurrency: currencySelector.options[currencySelector.selectedIndex].text,
            totalTax : getPriceWithCurrency(document, prTotalTaxCur, prTotalTaxVal),
            taxes : priceDetailsArray
        };
    } else {
        return null;
    };
};

/*
 * Gets flight leg details of flight if data exists
 * @return flightItinerary object as JSON
 */
function getFlightLegDetails(){
    function getFlightLeg(flightLegDetails) {
        return x =  {
            lightLeg : getClassText(flightLegDetails, flFlightLeg),
            departureSiteName : getClassText(flightLegDetails, flDepatureSite),
            departureSiteCode : getClassText(flightLegDetails, flDepatureSite),
            departureTime : getClassText(flightLegDetails, flDepatureTime),
            arrivalSiteName : getClassText(flightLegDetails, flArrivalSite),
            arrivalSiteCode : getClassText(flightLegDetails, flArrivalSite),
            arrivalTime : getClassText(flightLegDetails, flArrivalTime), 
            flightDuration : getClassText(flightLegDetails, flDuration),
            flightDate : getClassText(flightLegDetails, flDate),
            flightAirline : getClassText(flightLegDetails, flAirline),
            flightCode : getClassText(flightLegDetails, flNumber),
            flightType : getClassText(flightLegDetails, flFlightType),
            fareType : getClassText(flightLegDetails, flFareType)
        };
    };

    var flFlightLeg = "itinerary";
    var flDepatureSite = "timeline-takeoff-locationcode ellipsis ";
    var flDepatureTime = "leftbound timeline-time timeline-time-departure ";
    var flArrivalSite = "timeline-landing-locationcode ellipsis ";
    var flArrivalTime = "rightbound timeline-time timeline-time-arrival";
    var flDuration = "timeline-flight-duration-textdate";
    var flDate = "date";
    var flAirline = "airlinename";
    var flNumber = "flightnumber";
    var flFlightType = "stop-number-text";
    var flFareType = "ff-name ff-label-exist";

    if (document.querySelector("#fare-review-bounds") !== null) {
        var flightLegDetailsArray = [];
        var flightLegNodes = document.getElementById("fare-review-bounds").getElementsByClassName("bounds-list ");
        for (i = 0; i < flightLegNodes.length; i++) {
            var flightLeg = getFlightLeg(flightLegNodes[i]);
            flightLegDetailsArray.push(flightLeg);
        };
        return flightLegDetailsArray;
    } else {
        return null;
    };
};

/*
 * Gets services details of flight if data exists
 * @return serviceItinerary object as JSON
 */
function getServiceDetails(){
    function getServiceLeg(serviceDetails){
        var serviceHeadings = serviceDetails.getElementsByClassName("servicesbreakdown-bound-category-elem servicesbreakdown-category-elem");
        var serviceDetailList = serviceDetails.getElementsByClassName("servicesbreakdown-bound-list servicesbreakdown-list");
        var serviceLegArray = [];
    
        var leg = getClassText(serviceDetails, seServiceLegText);
        var legDateTime = getClassText(serviceDetails, seServiceLegDate);
    
        for (j = 0; j < serviceHeadings.length; j++) {
            var heading = serviceHeadings[j].innerText;
            var serviceDetail = serviceDetailList[j].getElementsByClassName("servicesbreakdown-bound-elem servicesbreakdown-elem row");
    
            for (k = 0; k < serviceDetail.length; k++) {
                var detailDesc = getClassText(serviceDetail[k], seServiceName);
                var detailPrice = getPriceWithCurrency(serviceDetail[k], sePriceCur, sePriceVal);
    
                var serviceInstance = {
                    type : heading,
                    description : detailDesc,
                    price : detailPrice
                }
                serviceLegArray.push(serviceInstance);
            }
        }
        return {
            flightLeg : leg,
            flightDate : legDateTime,
            serviceDetails : serviceLegArray
        };
    }
    //Service Literals
    var sePriceCur = "service-breakdown-item-currency";
    var sePriceVal = "service-breakdown-item-price";
    var seServiceLegText = "servicesbreakdown-bound-header-from-to";
    var seServiceLegDate ="services-breakdown-begin-date";
    var seServiceName = "servicesbreakdown-service-name col-xs-13 col-sm-18";
    
    if(document.getElementsByClassName("servicesbreakdown-panel-body panel-body").length > 0) {
        var seviceDetailsObj = [];
        var serviceDetailsNodes = document.getElementById("tpl10_extended-tripsummary_contentSctn").getElementsByClassName("servicesbreakdown-bound-wrapper refund-wrapper");
    
        for (i = 0; i < serviceDetailsNodes.length; i++) {
            var serviceLeg = getServiceLeg(serviceDetailsNodes[i]);
            seviceDetailsObj.push(serviceLeg);
        };
        return {
            totalPrice : getPriceWithCurrency(document, sePriceCur, sePriceVal),
            services : seviceDetailsObj
        };
    } else {
        return null;
    };
};

/*
 * Gets passenger details of flight if data exists
 * @return carItinerary object as JSON 
 */
function getPassengerDetails(){

    //Gets individual passenger details
    function getPassenger(passengerDetails){
        if(typeof(passengerDetails) !== undefined) {
            var passengerNode = {
                name : getClassText(passengerDetails, trName),
                type : getClassText(passengerDetails, trType)
            };
            return passengerNode;
        } else {
            return null;
        };
    };

    //Passenger Literals
    var trEmail = "traveller-contact-email";
    var trTelephone = "traveller-contact-phone-mobile";
    var trTelephoneNotify = "traveller-contact-phone-notif";
    var trName = "traveller-details-text traveller-name";
    var trType = "traveller-details-text traveller-type";
    
    if (document.querySelector("#fare-review-travellers") !== null) {
        var passengerListObj = [];
        var passengerNodes = document.getElementById("fare-review-travellers").getElementsByClassName("traveller-details small-text");
        for (i = 0; i < passengerNodes.length; i++) {
            var passenger = getPassenger(passengerNodes[i]);
            passengerListObj.push(passenger);
        };  
    
        var orderDetailNodes = document.getElementById("fare-review-travellers").getElementsByClassName("traveller-contact")[0];
        return {
            passangers : passengerListObj,
            bookingEmail : getClassText(orderDetailNodes, trEmail),
            bookingTelephone : getClassText(orderDetailNodes, trTelephone),
            bookingTelephoneNotify : getClassText(orderDetailNodes, trTelephoneNotify)
        };
    } else{
        return null;
    };
};

/*
 * Gets car details of flight if data exists
 * @return passengerItinerary object as JSON 
 */
function getCarDetails(){

    function getCarPriceBreakdown(carPriceDetails){
        var taxesNode = [];
        var nodes = carPriceDetails[0].getElementsByClassName("price-taxes");

        for (n = 0; n < nodes.length; n++) {
            var taxesDetails = {
               details : getClassText(nodes[n], caCarTaxDetails),
               price : getPriceWithCurrency(nodes[n], caCarTaxPriceCur, caCarTaxPricVal)
            }
            taxesNode.push(taxesDetails);

        }

        return {
            baseRate : getPriceWithCurrency(carPriceDetails[0], caCarBasePriceCur, caCarBasePriceVal),
            chargeBreakdown : taxesNode
        };
    };
    //Car Literals
    var caCarName = "name";
    var caCarType = "category";
    var caCarPriceCur = "price-currency menu-item-amount-currency";
    var caCarPriceVal = "price-value menu-item-amount-value";
    var caCarBasePriceCur = "price-currency base-rate-amount-currency-taxes";
    var caCarBasePriceVal = "price-value base-rate-amount-value-taxes";
    var caCarTaxDetails = "price-mini-breakdown-text price-detail-text";
    var caCarTaxPriceCur = "price-currency price-detail-amount-currency";
    var caCarTaxPricVal = "price-value price-detail-amount-value";

    if (document.querySelector("#car-details-container") !== null) {
        var carDetails = document.getElementById("car-details-container");
        var carPriceNode = document.getElementById("price-details-car-tpl31_").getElementsByClassName("price-amount menu-item-amount");
        var carPirceBreakdown = document.getElementById("price-details-car-tpl31_-collapsible").getElementsByClassName("breakdown");

        return {
            carName : getClassText(carDetails, caCarName),
            carType : getClassText(carDetails, caCarType),
            carTotalPrice : getPriceWithCurrency(carPriceNode[0], caCarPriceCur, caCarPriceVal),
            carPriceBreakdown : getCarPriceBreakdown(carPirceBreakdown)
        };
    } else{
        return null;
    };
};